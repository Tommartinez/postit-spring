[1mdiff --git a/Task.mv.db b/Task.mv.db[m
[1mindex 1365541..6ddbaff 100644[m
Binary files a/Task.mv.db and b/Task.mv.db differ
[1mdiff --git a/src/main/java/app/controllers/IndexController.java b/src/main/java/app/controllers/IndexController.java[m
[1mindex ffe96ca..1064a60 100644[m
[1m--- a/src/main/java/app/controllers/IndexController.java[m
[1m+++ b/src/main/java/app/controllers/IndexController.java[m
[36m@@ -2,9 +2,12 @@[m [mpackage app.controllers;[m
 [m
 import app.entities.Category;[m
 import app.entities.Task;[m
[32m+[m[32mimport app.entities.User;[m
 import app.repositories.CategoryRepository;[m
 import app.repositories.TaskRepository;[m
[32m+[m[32mimport app.repositories.UserRepository;[m
 import org.springframework.beans.factory.annotation.Autowired;[m
[32m+[m[32mimport org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;[m
 import org.springframework.stereotype.Controller;[m
 import org.springframework.ui.Model;[m
 import org.springframework.web.bind.annotation.*;[m
[36m@@ -18,6 +21,9 @@[m [mpublic class IndexController {[m
     @Autowired[m
     private CategoryRepository categoryRepository;[m
 [m
[32m+[m[32m    @Autowired[m
[32m+[m[32m    private UserRepository userRepository;[m
[32m+[m
     @GetMapping(path="/tasks")[m
     public String  getAllTasks(Model model) {[m
         model.addAttribute("rows", taskRepository.findAll());[m
[36m@@ -61,5 +67,18 @@[m [mpublic class IndexController {[m
     public String newTask(Model model){[m
         return "new";[m
     }[m
[32m+[m
[32m+[m[32m    @GetMapping(value="/account/create")[m
[32m+[m[32m    public String newAcc(@RequestParam("username")String username,[m
[32m+[m[32m                         @RequestParam("password")String password,[m
[32m+[m[32m                         @RequestParam("cpassword") String cpassword,[m
[32m+[m[32m                         Model model) {[m
[32m+[m[32m        if (password.equals(cpassword)) {[m
[32m+[m[32m            String mdpe = new BCryptPasswordEncoder().encode(password);[m
[32m+[m[32m            User u = new User("username", "{bcrypt}"+mdpe);[m
[32m+[m[32m            userRepository.save(u);[m
[32m+[m[32m        }[m
[32m+[m[32m        return "hello";[m
[32m+[m[32m    }[m
 }[m
 [m
[1mdiff --git a/src/main/resources/templates/tasks.html b/src/main/resources/templates/tasks.html[m
[1mindex 778ee21..6abb776 100644[m
[1m--- a/src/main/resources/templates/tasks.html[m
[1m+++ b/src/main/resources/templates/tasks.html[m
[36m@@ -23,6 +23,8 @@[m
   </div>[m
 </form>[m
 [m
[32m+[m[32m<br><br><br><br><br>[m
[32m+[m
 <h2>Create new account</h2>[m
 <form action="/account/create" method="post">[m
   <div class="button">[m
