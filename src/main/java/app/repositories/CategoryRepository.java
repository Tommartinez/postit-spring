package app.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import app.entities.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository
        extends CrudRepository<Category, Long> {
}