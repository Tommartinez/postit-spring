package app.controllers;

import app.entities.Category;
import app.entities.Task;
import app.entities.User;
import app.repositories.CategoryRepository;
import app.repositories.TaskRepository;
import app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class IndexController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping(path="/tasks")
    public String  getAllTasks(Model model) {
        model.addAttribute("rows", taskRepository.findAll());
        return "tasks";
    }

    @PostMapping(path="/tasks")
    public String  nouveau(@RequestParam("content")String content, @RequestParam("category")String category, Model model) {
        Category newcategory = new Category(category);
        Task task = new Task(newcategory, content);
        categoryRepository.save(newcategory);
        taskRepository.save(task);
        model.addAttribute("tasks", taskRepository.findAll());
        return "redirect:/tasks";
    }

    @GetMapping(value = "/tasks/{id}")
    public String getTask(@PathVariable("id") int id, Model model) {
        model.addAttribute("task", taskRepository.findById((long) id).get());
        return "task";
    }

    @PostMapping(value = "/tasks/{id}/delete")
    public String deleteTask(@PathVariable("id") int id, Model model) {
        System.out.println("hiii");
        taskRepository.deleteById((long)id);
        return "redirect:/tasks";
    }

    @GetMapping(value="/hello")
    public String hello(Model model){
        return "hello";
    }

    @GetMapping(value="/")
    public String defaultHello(Model model){
        return "redirect:/hello";
    }

    @GetMapping(value="/tasks/new")
    public String newTask(Model model){
        return "new";
    }

    @GetMapping(value="/account/create")
    public String newAcc(@RequestParam("username")String username,
                         @RequestParam("password")String password,
                         @RequestParam("cpassword") String cpassword,
                         Model model) {
        if (password.equals(cpassword)) {
            String mdpe = new BCryptPasswordEncoder().encode(password);
            User u = new User("username", "{bcrypt}"+mdpe);
            userRepository.save(u);
        }
        return "hello";
    }
}

